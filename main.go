package random

import (
	"fmt"
	"math/rand"
	"time"
)

var prefix = [...]string{"big", "small", "tiny", "angry", "sleepy"}
var pet = [...]string{"cat", "dog", "fish", "elephant", "mouse"}

func PetName(separator string) string {
	rand.Seed(time.Now().UnixNano())
	return fmt.Sprintf("%s%s%s", prefix[rand.Intn(len(prefix))], separator, pet[rand.Intn(len(pet))])
}
